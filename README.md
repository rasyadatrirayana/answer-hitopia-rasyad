Pada nomor 3 saya menggunakan 1 for looping untuk melakukan pengecekan per karakternya dan juga stack agar lebih mudah membandingkan antara karakter kurung buka dan tutupnya. Sehingga dalam analisisnya : 

1. input : panjang string yg berisi tanda kurung adalah n
2. setiap karakter dalam string diperiksa satu per satu dalam loop for pada fungsi isBalanced. Operasi ini memiliki kompleksitas O(n), karena harus memeriksa setiap karakter dalam string
3. Untuk setiap karakter dalam string, terdapat operasi push atau pop pada stack. Push dan pop pada stack memiliki kompleksitas waktu O(1)

Oleh karena ini kompleksitas waktu pada keseluruhan kode ini adalah O(n), di mana n ini adalah panjang string yg berisi tanda kurung.
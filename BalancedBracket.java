import java.util.*;

public class BalancedBracket {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Enter a bracket sequence: ");
        String s = input.nextLine().replaceAll("\\s", "");
        
        if (isBalanced(s)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    public static boolean isBalanced(String s) {
        Stack<Character> stack = new Stack<>();
        
        for (char c : s.toCharArray()) {
            if (c == '{' || c == '[' || c == '(') {
                stack.push(c);
            } else if (c == '}' || c == ']' || c == ')') {
                if (stack.isEmpty()) {
                    return false;
                }
                
                char top = stack.pop();
                if ((c == '}' && top != '{') || 
                    (c == ']' && top != '[') ||
                    (c == ')' && top != '(')) {
                    return false;
                }
            }
        }
        
        return stack.isEmpty();
    }

}

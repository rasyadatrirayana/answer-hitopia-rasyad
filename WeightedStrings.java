import java.util.*;

public class WeightedStrings {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        String isRepeat = "";

        do{

            isRepeat = "";

            String s = "";
            ArrayList<Integer> queries = new ArrayList<>();
            int queryLength = 0;
            List<String> result = new ArrayList<>();

            System.out.print("Masukkan string : ");
            s = input.next();

            ArrayList<Integer> stringValue = calculateStringValue(s);

            System.out.print("Masukkan panjang query : ");
            queryLength = input.nextInt();

            for (int i = 0; i < queryLength; i++) {

                int query = 0;
                System.out.printf("Masukkan query ke-%s : ", i+1);
                query = input.nextInt();

                queries.add(query);

            }

            for (int num : queries) {
                if (contains(stringValue, num) || containsConsecutiveSubarray(stringValue, num)) {
                    result.add("Yes");
                } else {
                    result.add("No");
                }
            }

            System.out.print("result : ");

            for (String item : result) {
                System.out.print(item + ", ");
            }

            System.out.print("\ntry again? : (y/n)  : ");
            isRepeat = input.next();

        }
        while(isRepeat.equalsIgnoreCase("y"));

        input.close();

    }

    public static ArrayList<Integer> calculateStringValue(String s){
        char[] splittedString = s.toCharArray();
        ArrayList<Integer> stringValue = new ArrayList<>();

        for(char charItem : splittedString){
            Integer charValue = Character.toLowerCase(charItem) - 'a' + 1;
            stringValue.add(charValue);
        }

        return stringValue;

    }

    public static boolean contains(ArrayList<Integer> array, int target) {
        for (int num : array) {
            if (num == target) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsConsecutiveSubarray(ArrayList<Integer> array, int target) {

        Integer temp = array.get(0);

        for (int i = 1; i < array.size(); i++) {
            if(array.get(i).equals(array.get(i-1))){
                temp = temp + array.get(i);
            }
            else{
                temp = array.get(i);
            }

            if(temp == target){
                return true;
            }

        }

        return false;
    }

}
import java.util.*;

public class HighestPalindrome {
	public static void main(String[] args){

       	Scanner input = new Scanner(System.in);
        	String isRepeat = "";

        	do{

            		String s = "";
            		int k = 0;

            		System.out.print("masukkan string number: ");
            		s = input.next();
            		System.out.print("k : ");
            		k = input.nextInt();

            		System.out.println();

            		String palindrome = makePalindrome(s, k);
            		System.out.println("highest palindrome : " + palindrome);

            		System.out.print("\nCoba lagi?(y/n)  : ");
            		isRepeat = input.next();

        	}while(isRepeat.equalsIgnoreCase("y"));

    	}

    	public static String makePalindrome(String input, int k) {
        	return makePalindromeRecursive(input.toCharArray(), k, 0, input.length() - 1);
    	}

	public static String makePalindromeRecursive(char[] chars, int k, int left, int right) {
        	if (left >= right) {
            		return new String(chars);
        	}

		if (chars[left] != chars[right]) {
            		char maxChar = (char) findMax(chars[left], chars[right]);
	            	chars[left] = maxChar;
       	     	chars[right] = maxChar;
            		k--;
		}

        	if (k < 0) {
	            return "-1";
        	}

	        return makePalindromeRecursive(chars, k, left + 1, right - 1);
    	}

    	public static int findMax(int a, int b) {
    	    return a > b ? a : b;
    	}

}
